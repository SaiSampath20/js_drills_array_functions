function filter(elements, cb) {
    arr = [];
    if (elements == undefined || cb == undefined)
        return arr;
    for (let i = 0; i < elements.length; i++) {
        if (cb(each(elements, i)) == true)
            arr.push(elements[i]);
    }
    return arr;
}

const each = (elements, i) => {
    return elements[i];
}

module.exports = { filter, each };