function flat(elements, array) {
    for (let i = 0; i < elements.length; i++) {
        if (Array.isArray(elements[i]))
            flat(elements[i], array);
        else
            array.push(elements[i]);
    }
    return undefined;
}
function flatten(elements) {
    array = [];
    if (elements == undefined)
        return array;
    flat(elements, array);
    return array;
}

module.exports = flatten