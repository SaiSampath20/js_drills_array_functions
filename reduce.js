function reduce(elements, cb, startingValue) {
    if (elements == undefined || cb == undefined)
        return undefined;
    if (startingValue == undefined) {
        startingValue = elements[0];
        for (let i = 1; i < elements.length; i++) {
            startingValue = cb(startingValue, elements[i], i, elements);
        }
    }
    else {
        for (let i = 0; i < elements.length; i++) {
            startingValue = cb(startingValue, elements[i], i, elements);
        }
    }
    return startingValue;
}

module.exports = reduce;