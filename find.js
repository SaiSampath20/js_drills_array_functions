const filter = require("./filter");

function find(elements, cb) {
    if (elements == undefined || cb == undefined)
        return undefined;
    for (let i = 0; i < elements.length; i++) {
        if (cb(filter.each(elements, i)) == true)
            return elements[i];
    }
    return undefined;
}

module.exports = find;