const map = require("../map");

const items = [1, 2, 3, 4, 5, 5];

// TestCase 1
const ans = map(items, (num, index, items) => {
    const a = 10, b = 20, c = 30;
    num += a;
    num -= b;
    num += c;
    return num;
});

console.log(ans);

// TestCase 2
console.log(map(items));

// TestCase 3
console.log(map([], (num) => {
    const a = 10, b = 20, c = 30;
    num += a;
    num -= b;
    num += c;
    return num;
}));

// TestCase 4
console.log(map());