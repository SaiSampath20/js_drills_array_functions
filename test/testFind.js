const find = require("../find");

const items = [1, 2, 3, 4, 5, 5];

// TestCase 1
const ans = find(items, (num) => {
    return num > 4;
});

console.log(ans);

// TestCase 2
console.log(find(items));

// TestCase 3
console.log(find([], (num) => {
    return num > 4;
}));

// TestCase 4
console.log(find());