const each = require("../each");

const items = [1, 2, 3, 4, 5, 5];

// TestCase 1
each(items, (items, i) => {
    return items[i];
})

// TestCase 2
console.log(each(items));

// TestCase 3
console.log(each([], (num) => {
    return num > 2;
}));

// TestCase 4
console.log(each());