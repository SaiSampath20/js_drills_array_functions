const { filter } = require("../filter");

const items = [1, 2, 3, 4, 5, 5];

// TestCase 1
const ans = filter(items, (num) => {
    return num > 2;
});

console.log(ans);

// TestCase 2
console.log(filter(items));

// TestCase 3
console.log(filter([], (num) => {
    return num > 2;
}));

// TestCase 4
console.log(filter());