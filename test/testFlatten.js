const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];

// TestCase 1
const arr = flatten(nestedArray);

console.log(arr);

// TestCase 2
console.log(flatten());