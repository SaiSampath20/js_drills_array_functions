const reduce = require("../reduce");

const items = [1, 2, 3, 4, 5, 5];

// TestCase 1
const ans = reduce(items, (accumulator, currentValue, currentIndex, items) => {
    return accumulator + currentValue;
}, 10);

console.log(ans);

// TestCase 2
console.log(reduce(items));

// TestCase 3
console.log(reduce([], (accumulator, currentValue) => {
    return accumulator > currentValue ? accumulator : currentValue;
}));

// TestCase 4
console.log(reduce());