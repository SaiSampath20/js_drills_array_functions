function each(elements, cb) {
    if (elements == undefined || cb == undefined)
        return undefined;
    for (let i = 0; i < elements.length; i++)
        console.log(cb(elements, i));
    return undefined;
}

module.exports = each;