function map(elements, cb) {
    arr = [];
    if (elements == undefined || cb == undefined)
        return arr;
    for (let i = 0; i < elements.length; i++)
        arr.push(cb(elements[i], i, elements));
    return arr;
}

module.exports = map;